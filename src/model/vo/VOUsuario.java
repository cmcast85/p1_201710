package model.vo;

import api.IComparable;

public class VOUsuario implements IComparable<VOUsuario> {

	private long idUsuario;	
	
	private long primerTimestamp;
	
	private int numRatings;
	
	private double diferenciaOpinion;
	
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getPrimerTimestamp() {
		return primerTimestamp;
	}
	public void setPrimerTimestamp(long primerTimestamp) {
		this.primerTimestamp = primerTimestamp;
	}
	public int getNumRatings() {
		return numRatings;
	}
	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}
	public double getDiferenciaOpinion() {
		return diferenciaOpinion;
	}
	public void setDiferenciaOpinion(double diferenciaOpinion) {
		this.diferenciaOpinion = diferenciaOpinion;
	}
	@Override
	public int compareTo(VOUsuario o, int criterio) {
		switch(criterio){
		case 0 : return (int)(idUsuario - o.idUsuario);
		case 1 : return (int)(primerTimestamp - o.primerTimestamp);
		case 2 : return numRatings - o.numRatings;
		case 3 : return (int)((diferenciaOpinion - o.diferenciaOpinion)*1000);
		default : return 0;
		}
	}
	
}
