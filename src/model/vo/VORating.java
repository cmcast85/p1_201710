package model.vo;

import api.IComparable;

public class VORating implements IComparable<VORating>{
	
	private long idUsuario;
	private long idPelicula;
	private double rating;
	private long timestamp;
	
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public double getTimestamp() {
		return timestamp;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	@Override
	public int compareTo(VORating o, int criterio) {
		switch(criterio){
		case 0 : return (int)(idUsuario - o.idUsuario);
		case 1 : return (int)(idPelicula - o.idPelicula);
		case 2 : return (int)(rating - o.rating);
		case 3 : return (int)(timestamp - o.timestamp);
		default : return 0;
		}
	}
}
