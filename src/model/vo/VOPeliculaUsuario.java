package model.vo;

import model.data_structures.ILista;

public class VOPeliculaUsuario {
	
	private long idPelicula;
	private double ratingUsuario;
	private ILista<VOUsuario> usuariosRecomendados;
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public void setRatingUsuario(double rating) {
		ratingUsuario = rating;
	}
	public double getRatingUsuario() {
		return ratingUsuario;
	}
	public ILista<VOUsuario> getUsuariosRecomendados() {
		return usuariosRecomendados;
	}
	public void setUsuariosRecomendados(ILista<VOUsuario> usuariosRecomendados) {
		this.usuariosRecomendados = usuariosRecomendados;
	}

}
