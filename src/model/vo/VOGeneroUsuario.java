package model.vo;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;

public class VOGeneroUsuario {
	
	private String genero;
	private ILista<VOUsuarioConteo> usuarios;
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public ILista<VOUsuarioConteo> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(ILista<VOUsuarioConteo> usuarios) {
		this.usuarios = usuarios;
	}
	public void addUsuario(VOUsuarioConteo usuario) {
		try {
			usuarios.agregarElementoFinal(usuario);
		} catch (NullPointerException e) {
			usuarios = new ListaEncadenada<VOUsuarioConteo>();
			usuarios.agregarElementoFinal(usuario);
		}
	}
}
