package model.vo;

import api.IComparable;
import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;

public class VOPelicula implements IComparable<VOPelicula>{
	
	private long idPelicula; 
	
	private String titulo;
	
	private int numeroRatings;
	
	private int numeroTags;
	
	private double promedioRatings;
	
	private int agnoPublicacion;

	private ILista<VOTag> tagsAsociados;
	
	private ILista<String> generosAsociados;
	
	private double diferencia;
	
	public void setDiferencia(double pdiferencia ){
		this.diferencia = pdiferencia;
	}
	
	public double getDiferencia(){
		return diferencia;
	}
	
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public int getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<VOTag> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<VOTag> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public void setTagsAsociados(VOTag tagAsociado) {
		try {
			this.tagsAsociados.agregarElementoFinal(tagAsociado);
		} catch (NullPointerException e) {
			this.tagsAsociados = new ListaEncadenada<VOTag>();
			this.tagsAsociados.agregarElementoFinal(tagAsociado);
		}
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}

	@Override
	public int compareTo(VOPelicula o, int criterio) {
		switch(criterio){
		case -3 : return o.numeroRatings - numeroRatings;
		case -2 : return (int) ((o.promedioRatings - promedioRatings)*1000);
		case -1 : return o.agnoPublicacion - agnoPublicacion;
		case 0 : return titulo.compareTo(o.titulo);
		case 1 : return agnoPublicacion - o.agnoPublicacion;
		case 2 : return (int) ((promedioRatings - o.promedioRatings)*1000);
		case 3 : return numeroRatings - o.numeroRatings;
		case 4 : return tagsAsociados.darNumeroElementos() - o.tagsAsociados.darNumeroElementos();
		case 5 : return (int)(idPelicula - o.idPelicula);
		case 6 : return (int)((diferencia - o.diferencia)*1000);
		default : return 0;
		}
	}
	
	@Override
	public String toString() {
		return (titulo + " id: " + idPelicula + " (" + agnoPublicacion + ")");
	}
}
