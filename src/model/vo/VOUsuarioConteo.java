package model.vo;

import api.IComparable;

public class VOUsuarioConteo implements IComparable<VOUsuarioConteo>{
	private long idUsuario;
	
	private int conteo;
	
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getConteo() {
		return conteo;
	}
	public void setConteo(int conteo) {
		this.conteo = conteo;
	}
	@Override
	public int compareTo(VOUsuarioConteo o, int criterio) {
		switch(criterio){
		case -1: return o.conteo - conteo;
		case 0 : return conteo - o.conteo;
		case 1 : return (int)(idUsuario - o.idUsuario);
		default : return 0;
		}
	}
	
}
