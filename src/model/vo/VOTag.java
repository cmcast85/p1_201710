package model.vo;

import api.IComparable;

public class VOTag implements IComparable<VOTag> {
	
	private String tag;
	
	private long timestamp;
	
	private long userId;
	
	private long movieId;
	
	public String getTag() {
		return tag;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getMovieId() {
		return movieId;
	}
	public void setMovieId(long movieId) {
		this.movieId = movieId;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	@Override
	public int compareTo(VOTag o, int criterio) {
		switch (criterio) {
		case -1: return (int)(o.timestamp - timestamp);
		}
		return 0;
	}
	
}
