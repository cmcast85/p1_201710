package model.vo;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;

public class VOGeneroTag {

	private ILista<String> tags;
	private String genero;
	public ILista<String> getTags() {
		return tags;
	}
	public void setTags(ILista<String> tags) {
		this.tags = tags;
	}
	public void addTag(String tag) {
		try {
			tags.agregarElementoFinal(tag);
		} catch (NullPointerException e) {
			this.tags = new ListaEncadenada<String>();
			tags.agregarElementoFinal(tag);
		}
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
}
