package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private Nodo<T> cabeza;
	private Nodo<T> posicionActual;
	private Nodo<T> ultimoElemento;
	private int tamano;
	
	public ListaEncadenada (T primerElemento){
		cabeza = new Nodo<T>(primerElemento);
		posicionActual = cabeza;
		tamano = 1;
	}
	
	public ListaEncadenada (){
		tamano = 0;
		cabeza = null;
		posicionActual= null;
	}
	
	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			Nodo<T> actual = null;
			@Override
			public boolean hasNext() {
				if (actual == null || actual.darSiguiente() != null) return true;
				else return false;
			}

			@Override
			public T next() {
				if (actual == null) actual = cabeza;
				else actual = actual.darSiguiente();
				return actual.darObjeto();
			}
		};
	}

	@Override
	public void agregarElementoFinal(T elem) {
		if(cabeza==null){
			cabeza = new Nodo<T>(elem);
			posicionActual = cabeza;
			ultimoElemento = cabeza;
		}
		else {
			Nodo<T> actual = cabeza;
			
			while (actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
			}
			Nodo<T> siguiente = new Nodo<T>(elem);
			actual.cambiarSiguiente(siguiente);
			ultimoElemento = siguiente;
		}
		tamano++;
	}
	
	public void agregarElementoFinalNorepetido(T elem) {
		if(cabeza==null){
			cabeza = new Nodo<T>(elem);
			posicionActual = cabeza;
			tamano++;
		}
		else {
			Nodo<T> actual = cabeza;
			boolean repetido = false;
			while (actual.darSiguiente() != null && !repetido) {
				if(actual.darObjeto().equals(elem)){
					repetido = true;
				}
				actual = actual.darSiguiente();
			}
			if(!repetido){
				Nodo<T> siguiente = new Nodo<T>(elem);
				actual.cambiarSiguiente(siguiente);
				tamano++;
			}
		}
	}

	@Override
	public T darElemento(int pos) {
		Nodo<T> actual = cabeza;
		for (int i = 0; i < pos-1; i++) {
			if(actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
			}
			else return null;
		}
		return actual.darObjeto();
	}
	
	public T darUltimoElemento() {
		return ultimoElemento.darObjeto();
	}


	@Override
	public int darNumeroElementos() {
		return tamano;
	}

	public T darElementoPosicionActual() {
		return posicionActual.darObjeto();
	}
	
	public void restablecerActual() {
		posicionActual = cabeza;
	}

	public boolean avanzarSiguientePosicion() {
		if (posicionActual.darSiguiente() != null) {
			posicionActual = posicionActual.darSiguiente();
			return true;
		}
		else return false;
	}

	public boolean retrocederPosicionAnterior() {
		Nodo<T> anterior = cabeza;
		if (posicionActual.equals(cabeza)) {
			return false;
		}
		while (!anterior.darSiguiente().equals(posicionActual)) {
			anterior = anterior.darSiguiente();
		}
		posicionActual = anterior;
		return true;
	}
	
	@Override
	public boolean isEmpty()
	{
		return tamano==0;
	}

	public Nodo<T> darNodo(int pos) {
		Nodo<T> actual = cabeza;
		for (int i = 0; i < pos-1; i++) {
			if(actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
			}
			else return null;
		}
		return actual;
	}
	
	public boolean eliminarElemento(int pos) {
		Nodo<T> elem = darNodo(pos);
		Nodo<T> anterior = darNodo(pos-1);
		boolean logro = false;
		if (tamano > 1) {
			if (elem.darSiguiente() == null) {
				anterior.cambiarSiguiente(null);
				ultimoElemento = anterior;
				elem.cambiarSiguiente(null);
				logro = true;
			} else if (elem.darSiguiente() != null && anterior != null) {
				anterior.cambiarSiguiente(elem.darSiguiente());
				elem.cambiarSiguiente(null);
				logro = true;
			} else cabeza = elem.darSiguiente();
		}
		else {
			cabeza = null;
			posicionActual = null;
			ultimoElemento = null;
			logro = true;
		}
		if (logro) tamano--;
		return logro;
	}

	@Override
	public boolean contains(T gen) {
		Nodo<T> actual = cabeza;
		
		while (actual.darSiguiente() != null) {
			if(actual.darObjeto().equals(gen)){
				return true;
			}
			actual = actual.darSiguiente();
		}
		return false;
	}



}
