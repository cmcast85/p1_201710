package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.Queue;
import model.data_structures.Stack;
//import model.vo.VOAgnoPelicula;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;
import api.ISistemaRecomendacionPeliculas;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas {
	
	private Thread t;
	
	private ILista<VOPelicula> peliculas;
	
	private ILista<VORating> ratings;
	
	private ILista<VOTag> tags;
	
	//private ILista<VOAgnoPelicula> peliculasAgno;
	
	private ILista<VOGeneroPelicula> generoPelicula;
	
	private ILista<VOGeneroUsuario> generoUsuario;
	
	private ILista<VOUsuario> usuarios;
	
	private Stack<VOOperacion> operaciones;
	
	long ultimoId;
	
	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {
		String temp = null;
	    String[] temp2 = new String[3];
	    ThreadLoading loading = new ThreadLoading();
	    loading.start();
	    ultimoId = 0;
	    
	    String nombrePelicula = null;
	    int anoPelicula;
	    ListaEncadenada<String> listaGeneros;
	    
	    peliculas = new ListaEncadenada<VOPelicula>();
	    VOPelicula pelicula = new VOPelicula();
	    int numGeneros;
	    
	    try {
	      BufferedReader br = new BufferedReader(new FileReader(rutaPeliculas));
	      //primera linea no tiene informacion
	      br.readLine();
	      
	      while((temp = br.readLine()) != null) {

	        //revisa si tiene " en el nombre
	        if(temp.contains("\"")) {
	          temp2[0] = temp.substring(0, temp.indexOf(','));
	          temp2[1] = temp.substring(temp.indexOf(',')+2, temp.lastIndexOf(',')-1);
	          temp2[2] = temp.substring(temp.lastIndexOf(',')+1, temp.length());
	        }
	        else {
	          temp2 = temp.split(",");
	        }
	        
	        //Saca los atributos de la pelicula
	        nombrePelicula = temp2[1];
	        
	        try {
	          anoPelicula = Integer.parseInt(nombrePelicula.substring(nombrePelicula.lastIndexOf('(')+1, nombrePelicula.lastIndexOf(')')));
	        } catch (NumberFormatException | StringIndexOutOfBoundsException e) {
	          if (nombrePelicula.endsWith(")")) {
	            anoPelicula = Integer.parseInt(nombrePelicula.substring(nombrePelicula.lastIndexOf('(')+1, nombrePelicula.lastIndexOf('-')));
	          } else anoPelicula = 0;
	        }
	        if (nombrePelicula.contains("(") && nombrePelicula.matches(".*\\d+.*")) nombrePelicula = nombrePelicula.substring(0, nombrePelicula.lastIndexOf('(')-1);
	        
	        numGeneros = 1;
	        for (int i = 0; i < temp2[2].length(); i++) {
	          if(temp2[2].charAt(i) == '|') numGeneros++;
	        }
	        String[] temp3 = new String[numGeneros];
	        temp3 = temp2[2].split("\\|");
	        listaGeneros = new ListaEncadenada<String>();
	        for (int i = 0; i < temp3.length; i++) {
	          listaGeneros.agregarElementoFinal(temp3[i]);
	        }
	        if (Long.parseLong(temp2[0])>ultimoId) ultimoId = Long.parseLong(temp2[0]);
	        
	        //agrega la pelicula a la lista
	        pelicula = new VOPelicula();
	        pelicula.setIdPelicula(Long.parseLong(temp2[0]));
	        pelicula.setAgnoPublicacion(anoPelicula);
	        pelicula.setTitulo(nombrePelicula);
	        pelicula.setGenerosAsociados(listaGeneros);
	        
	        //System.out.println(nombrePelicula + ": " + a�oPelicula + ": " + listaGeneros.darElemento(0));
	        peliculas.agregarElementoFinal(pelicula);
	      }
	      br.close();
	      
	    } catch (IOException e) {
	      return false;
	    }
	    /*
	    //Carga la lista de a�os con las peliculas publicadas por a�o
	    VOAgnoPelicula ano;
	    peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();
	    for (int i = 0; i < 67; i++) {
	      ano = new VOAgnoPelicula();
	      ano.setAgno(1950 + i);
	    
	      
	      ILista<VOPelicula> tempo ;
	      tempo = new ListaDobleEncadenada<VOPelicula>();
	      for( VOPelicula iter : peliculas ){
	        if(iter.getAgnoPublicacion() == 1950 + i){
	          tempo.agregarElementoFinal(iter);
	        }
	      }
	      
	      ano.setPeliculas(tempo);
	      peliculasAgno.agregarElementoFinal(ano);
	    }*/
	    
	    actualizarGeneros();
	    
	    return true;
	}
	
	public void actualizarGeneros() {
		//Carga la lista de generos y las peliculas de ese genero
		
	    ILista<String> listagen = new ListaEncadenada<String>();
	    for(VOPelicula iter : peliculas){
	      for(String gen : iter.getGenerosAsociados()){
	        listagen.agregarElementoFinalNorepetido(gen);
	      }
	    }
	    
	    VOGeneroPelicula genero;
	    generoPelicula = new ListaEncadenada<VOGeneroPelicula>();
	    String ultimoGenero = "wololo";
	    for(String gn : listagen){
	    	if(!gn.equals(ultimoGenero)) {
		    	genero = new VOGeneroPelicula();
		    	genero.setGenero(gn);

				ILista<VOPelicula> tempo ;
				tempo = new ListaEncadenada<VOPelicula>();
				for( VOPelicula iter : peliculas ){
					for(String t : iter.getGenerosAsociados()){
						if(t.equals(gn)) tempo.agregarElementoFinalNorepetido(iter);
					}
				}
				genero.setPeliculas(tempo);
				generoPelicula.agregarElementoFinal(genero);
	    	}
	    	ultimoGenero = gn;
	    }
	}
	
	@Override
	public boolean cargarRatingsSR(String rutaRatings) {

		try {
			BufferedReader br = new BufferedReader(new FileReader(rutaRatings) );
			String temp;
			usuarios = new ListaEncadenada<VOUsuario>();
			ratings = new ListaEncadenada<VORating>();
			br.readLine();
			while((temp = br.readLine()) != null){
				String[] arr = temp.split(",");
				
				VORating rating = new VORating();
				
				rating.setIdUsuario(Integer.parseInt(arr[0]));
				rating.setIdPelicula(Long.parseLong(arr[1]) );
				rating.setRating(Double.parseDouble(arr[2]));
				rating.setTimestamp(Long.parseLong(arr[3]));
				
				ratings.agregarElementoFinal(rating);
			}

			br.close();
			
			cargarUsuariosGenero();
			
			long usuarioActual = 0;
			
			for (VORating rating : ratings) {

				//ratings y peliculas
				VOPelicula peli = buscarPelicula(rating.getIdPelicula());
				if (peli != null) {
					peli.setNumeroRatings(peli.getNumeroRatings() +1);
					
					double promedio = 0;
					promedio *=peli.getNumeroRatings() -1;
					
					promedio = (rating.getRating() + promedio)/peli.getNumeroRatings() ;
					
					peli.setPromedioRatings(promedio);
				}
				
				//Llena lista de usuarios
				if (rating.getIdUsuario() > usuarioActual) {
					usuarioActual = rating.getIdUsuario();
					VOUsuario usuario = new VOUsuario();
					usuario.setIdUsuario(usuarioActual);
					usuario.setPrimerTimestamp((long)rating.getTimestamp());
					usuario.setNumRatings(1);
					usuarios.agregarElementoFinal(usuario);
				}
				else {
					VOUsuario usuario = usuarios.darUltimoElemento();
					usuario.setNumRatings(usuario.getNumRatings()+1);
					if(usuario.getPrimerTimestamp() > rating.getTimestamp()){
						usuario.setPrimerTimestamp((long)rating.getTimestamp());
					}
				}
			}
			
			//System.out.println("Numero de usuarios: " + usuarios.darNumeroElementos());
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private void cargarUsuariosGenero() {
		//Llena lista de GeneroUsuarios
		t = new Thread() {
			public void run(){

				generoUsuario = new ListaEncadenada<VOGeneroUsuario>();
				for (VOGeneroPelicula genero: generoPelicula) {
					VOGeneroUsuario gen = new VOGeneroUsuario();
					gen.setGenero(genero.getGenero());
					generoUsuario.agregarElementoFinal(gen);
				}
				Sort<VORating> sortRatings = new Sort<>();
				ListaEncadenada<VORating> ratingsOrdenadosIdPelicula = (ListaEncadenada<VORating>) ratings;
				//ratingsOrdenadosIdPelicula = (ListaEncadenada<VORating>) sortRatings.mergeSort(ratingsOrdenadosIdPelicula, 0);
				ratingsOrdenadosIdPelicula = (ListaEncadenada<VORating>) sortRatings.mergeSort(ratingsOrdenadosIdPelicula, 1);
				Sort<VOPelicula> sortPeliculas = new Sort<>();
				ListaEncadenada<VOPelicula> peliculasOrdenadasId = (ListaEncadenada<VOPelicula>) peliculas;
				peliculasOrdenadasId = (ListaEncadenada<VOPelicula>) sortPeliculas.mergeSort(peliculasOrdenadasId, 5);
				for (VORating rating: ratingsOrdenadosIdPelicula) {
					while (rating.getIdPelicula() != peliculasOrdenadasId.darElementoPosicionActual().getIdPelicula()) {
						peliculasOrdenadasId.avanzarSiguientePosicion();
					}
					ListaEncadenada<String> generosPelicula = (ListaEncadenada<String>) peliculasOrdenadasId.darElementoPosicionActual().getGenerosAsociados();
					int i = 0;
					for (String genero : generosPelicula) {
						i = 0;
						generoUsuario.restablecerActual();
						while(!generoUsuario.darElementoPosicionActual().getGenero().equals(genero)) {
							generoUsuario.avanzarSiguientePosicion();
						}
						try {
							ListaEncadenada<VOUsuarioConteo> usuariosGen = (ListaEncadenada<VOUsuarioConteo>) generoUsuario.darElementoPosicionActual().getUsuarios();
							boolean encontro = false;
							usuariosGen.restablecerActual();
							while (!encontro) {
								long idBuscado = usuariosGen.darElemento(i).getIdUsuario();
								if( idBuscado == rating.getIdUsuario()) {
									int conteo = usuariosGen.darElemento(i).getConteo() + 1;
									usuariosGen.darElemento(i).setConteo(conteo);
									encontro = true;
								}
								i++;
							}
							if (!encontro) {
								VOUsuarioConteo usuarioCont = new VOUsuarioConteo();
								usuarioCont.setIdUsuario(rating.getIdUsuario());
								usuarioCont.setConteo(1);
								generoUsuario.darElementoPosicionActual().addUsuario(usuarioCont);
							}
						} catch (NullPointerException e) {
							VOUsuarioConteo usuarioCont = new VOUsuarioConteo();
							usuarioCont.setIdUsuario(rating.getIdUsuario());
							usuarioCont.setConteo(1);
							generoUsuario.darElementoPosicionActual().addUsuario(usuarioCont);
						}
					}
				}
				generoUsuario.restablecerActual();
			}
		};
		t.start();
	}
	
	private VOPelicula buscarPelicula(Long id) {
		
		for(VOPelicula peli : peliculas){
			if(peli.getIdPelicula() == id){
				return peli;
			}
		}
		return null;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		
		tags = new ListaEncadenada<VOTag>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(rutaTags) );
			String temp;
			br.readLine();
			while((temp = br.readLine()) != null){
				String[] arr = new String[4];
				boolean comillas = false;
				
				//revisa si tiene " en el nombre
		        if(temp.contains("\"")) {
		        	comillas = true;
		        	int primeraComa = temp.indexOf(',');
		        	int segundaComa = temp.indexOf(',', primeraComa+1);
					arr[0] = temp.substring(0, primeraComa);
					arr[1] = temp.substring(primeraComa + 1, segundaComa);
					arr[2] = temp.substring(segundaComa + 2, temp.lastIndexOf(',')-1);
					arr[3] = temp.substring(temp.lastIndexOf(',') + 1);
		        }
		        else {
		        	arr = temp.split(",");
		        }
				if (!comillas) {
					
				}
				String tagsTemp[] = arr[2].split(",");
				for (String nombreTag : tagsTemp) {
					VOTag tag = new VOTag();
					if(nombreTag.startsWith(" ")) nombreTag = nombreTag.substring(1);
					tag.setTag(nombreTag);
					tag.setTimestamp(Long.parseLong(arr[3]));
					tag.setUserId(Long.parseLong(arr[0]));
					tag.setMovieId(Long.parseLong(arr[1]));
					
					for(VOPelicula pel : peliculas){
						if(pel.getIdPelicula() == Long.parseLong(arr[1]) ){
							pel.setTagsAsociados(tag);
						}
					}
					 
					tags.agregarElementoFinal(tag);
					
					VOPelicula peli = buscarPelicula(Long.parseLong(arr[1]));
					if(peli != null) {
						peli.setNumeroTags(peli.getNumeroTags() + 1);
					}
				}
				
			}
			actualizarGeneros();
			br.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	  public int sizeMoviesSR() {
	    return peliculas.darNumeroElementos() ;
	  }

	  @Override
	  public int sizeUsersSR() {
		  return usuarios.darNumeroElementos();
	  }

	  @Override
	  public int sizeTagsSR() {
	    return tags.darNumeroElementos();
	  }

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		
		ILista<VOGeneroPelicula> ret = new ListaEncadenada<VOGeneroPelicula>();
		
		for(VOGeneroPelicula gen : generoPelicula){
			VOGeneroPelicula temp = new VOGeneroPelicula(); 
			temp.setGenero(gen.getGenero());
			
			Sort<VOPelicula> sort = new Sort<>();
			ILista<VOPelicula> temp2 = sort.mergeSort(gen.getPeliculas(), -2);
			
			int i = 0;
			Iterator<VOPelicula> iter = temp2.iterator();
			while(i<n&&iter.hasNext()){
				VOPelicula pel = iter.next();
				
				if(temp.getPeliculas()==null){
					ILista<VOPelicula> alv = new ListaEncadenada<VOPelicula>();
					alv.agregarElementoFinal(pel);
					temp.setPeliculas(alv);
					ret.agregarElementoFinal(temp);
				}
				else {
					ILista<VOPelicula> alv = temp.getPeliculas();
					alv.agregarElementoFinal(pel);
					temp.setPeliculas(alv);
					ret.agregarElementoFinal(temp);
				}
				i++;
			}
		}
		
		return ret;
	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		Sort<VOPelicula> sort = new Sort<VOPelicula>();
		ListaEncadenada<VOPelicula> retornar = (ListaEncadenada<VOPelicula>) peliculas;
		retornar = (ListaEncadenada<VOPelicula>) sort.mergeSort(retornar, 0);
		retornar = (ListaEncadenada<VOPelicula>) sort.mergeSort(retornar, 2);
		retornar = (ListaEncadenada<VOPelicula>) sort.mergeSort(retornar, 1);
		return retornar;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() 
	{
		ILista<VOGeneroPelicula> ret = new ListaEncadenada<VOGeneroPelicula>();
		for(VOGeneroPelicula gen : generoPelicula)
		{
			VOGeneroPelicula temp = new VOGeneroPelicula();
			temp.setGenero(gen.getGenero());
			
			Sort<VOPelicula> sort = new Sort<VOPelicula>();
			ILista<VOPelicula> temp2 = sort.mergeSort(gen.getPeliculas(), -2);
			
			ILista<VOPelicula> alv = new ListaEncadenada<VOPelicula>();
			alv.agregarElementoFinal(temp2.darElemento(0));
			temp.setPeliculas(alv);
			ret.agregarElementoFinal(temp);
		}
		return ret;
	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		Sort<VOPelicula> sort = new Sort<VOPelicula>();
		for(VOGeneroPelicula gen : generoPelicula){
			ILista<VOPelicula> pel = new ListaEncadenada<VOPelicula>();
			
			pel = sort.mergeSort(gen.getPeliculas(),1);
			gen.setPeliculas(pel);
			
			/*System.out.println("Genero: " + gen.getGenero());
			int i = 0;
			for (VOPelicula pelic:pel) {
				if (i%20 == 0) {
					System.out.println("\t" + pelic);
					if(pelic.getTagsAsociados() != null) {
						for (VOTag tag:pelic.getTagsAsociados()) {
								System.out.println("\t\t" + tag.getTag());
						}
					}
				}
				i++;
			}*/
		}
		return generoPelicula;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) 
	{
		Queue<VOPeliculaPelicula> queue = new Queue<VOPeliculaPelicula>();
		int i = 0;
		String temp = null;
		ILista<VOPeliculaPelicula> ret = new ListaEncadenada<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(rutaRecomendacion));
			while(i < n && (temp = br.readLine()) !=null){
				String[] as = temp.split(",");
				VOPeliculaPelicula pel = new VOPeliculaPelicula();
				pel.setPelicula(buscarPelicula(Long.parseLong(as[0])) );
				pel.setRating(Double.parseDouble(as[1]));
				queue.put(pel);
				i++;
			}
			br.close();
			
			
			VOPeliculaPelicula pelActual;
	        while((pelActual = queue.pull()) != null){
	            VOGeneroPelicula generoActual = buscarGenero(pelActual.getPelicula().getGenerosAsociados().darElemento(0));
	            for(VOPelicula pel: generoActual.getPeliculas()){
	            	double diferenciaOpinion = Math.abs(pel.getPromedioRatings() - pelActual.getRating());
	            	if(diferenciaOpinion <= 0.5){
	                    if(pelActual.getPeliculasRelacionadas()==null){
	                    	ILista<VOPelicula> temp2 = new ListaEncadenada<VOPelicula>();
	                    	pel.setDiferencia(diferenciaOpinion);
	                    	temp2.agregarElementoFinal(pel);
	                    	pelActual.setPeliculasRelacionadas(temp2);
	                    	
	                    }
	                    else {
	                    	pel.setDiferencia(diferenciaOpinion);
	                    	pelActual.getPeliculasRelacionadas().agregarElementoFinal(pel);
	                    }
	                }
	            }
	            Sort<VOPelicula> sort = new Sort<>();
	            
	            if(pelActual.getPeliculasRelacionadas() != null) {
	            	pelActual.setPeliculasRelacionadas(sort.mergeSort(pelActual.getPeliculasRelacionadas(), 6));
	            }
	            	
	            ret.agregarElementoFinal(pelActual);
	        }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		/*for (VOPeliculaPelicula pelpel : ret) {
			System.out.println(pelpel.getPelicula() + " Rating usuario: " + pelpel.getRating());
			for (VOPelicula pelicula : pelpel.getPeliculasRelacionadas()) {
				System.out.println("\tRating: " + pelicula.getPromedioRatings() + " | " + pelicula.getGenerosAsociados().darElemento(0) + "|" + pelicula);
			}
		}*/
		return ret;
	}

	private VOGeneroPelicula buscarGenero(String darElemento) {
		for(VOGeneroPelicula gen : generoPelicula){
			if(gen.getGenero().equals(darElemento)){
				return gen;
			}
		}
		return null;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		ListaEncadenada<VORating> retornar = new ListaEncadenada<>();
		for (VORating rating : ratings) {
			if (rating.getIdPelicula() == idPelicula) retornar.agregarElementoFinal(rating);
		}
		Sort<VORating> sort = new Sort<VORating>();
		retornar = (ListaEncadenada<VORating>) sort.mergeSort(retornar, 3);
		return retornar;
	}

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		//Espera al thread que llena generoUsuarios si sigue corriendo
		if(t.isAlive()) {
			System.out.println("Por favor espere mientras termina de cargarse el SR");
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		ListaEncadenada<VOGeneroUsuario> retornar = new ListaEncadenada<>();
		for (VOGeneroUsuario genero : generoUsuario) {
			//System.out.println("Genero: " + genero.getGenero());
			VOGeneroUsuario nuevo = new VOGeneroUsuario();
			nuevo.setGenero(genero.getGenero());
			ListaEncadenada<VOUsuarioConteo> usuarios = (ListaEncadenada<VOUsuarioConteo>) genero.getUsuarios();
			Sort<VOUsuarioConteo> usu = new Sort<>();
			if(usuarios != null) {
				usuarios = (ListaEncadenada<VOUsuarioConteo>) usu.mergeSort(usuarios, -1);
				for (int i = 1; i <= n; i++) {
					if (usuarios.darElemento(usuarios.darNumeroElementos() - i) != null) {
						nuevo.addUsuario(usuarios.darElemento(i));
						//System.out.println("Usuario: " + usuarios.darElemento(i).getIdUsuario() + "\t" + usuarios.darElemento(i).getConteo());
					}
				}
			}
			retornar.agregarElementoFinal(nuevo);
		}
		return retornar;
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		ListaEncadenada<VOUsuario> retornar = (ListaEncadenada<VOUsuario>) usuarios;
		Sort<VOUsuario> sort = new Sort<>();
		retornar = (ListaEncadenada<VOUsuario>) sort.mergeSort(retornar, 0);
		retornar = (ListaEncadenada<VOUsuario>) sort.mergeSort(retornar, 2);
		retornar = (ListaEncadenada<VOUsuario>) sort.mergeSort(retornar, 1);
		return retornar;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		ListaEncadenada<VOGeneroPelicula> retornar = new ListaEncadenada<>();
		for (VOGeneroPelicula genero : generoPelicula) {
			retornar.agregarElementoFinal(genero);
			ListaEncadenada<VOPelicula> peliculas = (ListaEncadenada<VOPelicula>) retornar.darElementoPosicionActual().getPeliculas();
			Sort<VOPelicula> sort = new Sort<>();
			peliculas = (ListaEncadenada<VOPelicula>) sort.mergeSort(peliculas, -3);
			ListaEncadenada<VOPelicula> peliculasTemp = new ListaEncadenada<>();
			for (int i  = 0; i < n; i++) {
				peliculasTemp.agregarElementoFinal(peliculas.darElemento(i));
			}
			retornar.darElementoPosicionActual().setPeliculas(peliculasTemp);
			retornar.avanzarSiguientePosicion();
		}
		return null;
	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		
		ListaEncadenada<VOUsuarioGenero> retornar = new ListaEncadenada<>();
		ListaEncadenada<VOGeneroTag> generoTags = new ListaEncadenada<>();
		
		long usuarioActual = 0;
		for (VOTag tag : tags) {
			if (tag.getUserId() > usuarioActual) {
				try {
					ListaEncadenada<VOGeneroTag> generosAnterior =  (ListaEncadenada<VOGeneroTag>) retornar.darUltimoElemento().getListaGeneroTags();
					SortString sortString = new SortString();
					for (VOGeneroTag genero : generosAnterior) {
						ListaEncadenada<String> aOrdenar = (ListaEncadenada<String>) genero.getTags();
						aOrdenar = (ListaEncadenada<String>) sortString.mergeSort(aOrdenar);
						genero.setTags(aOrdenar);
					}
				} catch (Exception e) {}
				usuarioActual = tag.getUserId();
				VOUsuarioGenero usuario = new VOUsuarioGenero();
				usuario.setIdUsuario(usuarioActual);
				generoTags = new ListaEncadenada<>();
				ListaEncadenada<String> gens = (ListaEncadenada<String>) encontrarGenerosPorId(tag.getMovieId());
				for (String genero : gens) {
					VOGeneroTag genTag = new VOGeneroTag();
					genTag.setGenero(genero);
					genTag.addTag(tag.getTag());
					generoTags.agregarElementoFinal(genTag);
				}
				usuario.setListaGeneroTags(generoTags);
				retornar.agregarElementoFinal(usuario);
			}
			else {
				VOUsuarioGenero usuario = retornar.darUltimoElemento();
				generoTags = (ListaEncadenada<VOGeneroTag>) usuario.getListaGeneroTags();
				ListaEncadenada<String> gens = (ListaEncadenada<String>) encontrarGenerosPorId(tag.getMovieId());
				boolean encontro = false;
				for (String genero : gens) {
					for (VOGeneroTag generoExistente : generoTags) {
						if (genero.equals(generoExistente.getGenero())) {
							generoExistente.addTag(tag.getTag());
							encontro = true;
						}
					}
					if (!encontro) {
						VOGeneroTag genTag = new VOGeneroTag();
						genTag.setGenero(genero);
						genTag.addTag(tag.getTag());
						generoTags.agregarElementoFinal(genTag);
					}
				}
				usuario.setListaGeneroTags(generoTags);
			}
		}

		ListaEncadenada<VOGeneroTag> generosAnterior =  (ListaEncadenada<VOGeneroTag>) retornar.darUltimoElemento().getListaGeneroTags();
		SortString sortString = new SortString();
		for (VOGeneroTag genero : generosAnterior) {
			ListaEncadenada<String> aOrdenar = (ListaEncadenada<String>) genero.getTags();
			aOrdenar = (ListaEncadenada<String>) sortString.mergeSort(aOrdenar);
			genero.setTags(aOrdenar);
		}
		//solo imprime para probar
		/*for(VOUsuarioGenero usr : retornar) {
			System.out.println("Id usuario " + usr.getIdUsuario());
			for (VOGeneroTag gent : usr.getListaGeneroTags()) {
				System.out.println(gent.getGenero());
				for (String tag : gent.getTags()) {
					System.out.println("\t" + tag);
				}
			}
		}*/
		return retornar;
	}
	
	private ILista<String> encontrarGenerosPorId(long idPelicula) {
		for (VOPelicula busq : peliculas) {
			if (idPelicula == busq.getIdPelicula()) return busq.getGenerosAsociados();
		}
		return null;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) {
		Queue<VOPeliculaUsuario> queue = new Queue<>();
		int i = 0;
		String temp = null;
		ILista<VOPeliculaUsuario> retornar = new ListaEncadenada<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(rutaRecomendacion));
			while((i < n) && ((temp = br.readLine()) != null)) {
				String[] as = temp.split(",");
				VOPeliculaUsuario peliculaUsuario = new VOPeliculaUsuario();
				peliculaUsuario.setIdPelicula(Long.parseLong(as[0]));
				peliculaUsuario.setRatingUsuario(Double.parseDouble(as[1]));
				queue.put(peliculaUsuario);
				i++;
			}
			br.close();
			
			
			VOPeliculaUsuario usuario;
	        while((usuario = queue.pull()) != null){
	        	ListaEncadenada<VOUsuario> usuariosParecidos = new ListaEncadenada<>();
	        	for (VORating rating : ratings) {
	        		if (usuario.getIdPelicula() == rating.getIdPelicula()) {
		        		double diferenciaOpinion = Math.abs(rating.getRating() - usuario.getRatingUsuario());
		        		if (diferenciaOpinion <= 0.5) {
		        			VOUsuario usuarioNuevo = new VOUsuario();
		        			usuarioNuevo.setIdUsuario(rating.getIdUsuario());
		        			usuarioNuevo.setDiferenciaOpinion(diferenciaOpinion);
		        			usuariosParecidos.agregarElementoFinal(usuarioNuevo);
		        		}
	        		}
	        	}
	            Sort<VOUsuario> sort = new Sort<>();
	            usuariosParecidos = (ListaEncadenada<VOUsuario>) sort.mergeSort(usuariosParecidos, 3);
            	ListaEncadenada<VOUsuario> usuariosParecidos2 = new ListaEncadenada<>();
	            for (int j = 1; j <= n; j++ ) {
	            	if(usuariosParecidos.darNumeroElementos()!=0) {
		            	usuariosParecidos2.agregarElementoFinal(usuariosParecidos.darElemento(j));
	            	} else usuariosParecidos2 = new ListaEncadenada<>();
	            }
	            usuario.setUsuariosRecomendados(usuariosParecidos2);
	            retornar.agregarElementoFinal(usuario);
	        }
	        
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		/*for(VOPeliculaUsuario us : retornar){
			System.out.println("Rating usuario: " + us.getRatingUsuario() + "| " + buscarPelicula(us.getIdPelicula()));
			if(us.getUsuariosRecomendados().darNumeroElementos() != 0) {
				for (VOUsuario usuario : us.getUsuariosRecomendados()) {
					System.out.println("\tDiferencia opinion: " + usuario.getDiferenciaOpinion() + " | IdUsuario: " + usuario.getIdUsuario());
				}
			}
		}*/
		return retornar;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		ListaEncadenada<VOTag> retornar = new ListaEncadenada<>();
		for (VOTag tag: tags) {
			if ((int)tag.getMovieId() == idPelicula) retornar.agregarElementoFinal(tag);
		}
		Sort<VOTag> sortTags = new Sort<>();
		retornar = (ListaEncadenada<VOTag>) sortTags.mergeSort(retornar, -1);
		/*System.out.println("Pelicula: " + buscarPelicula((long)idPelicula) + "   (" + idPelicula + ")");
		for (VOTag tag : retornar) {
			System.out.println("\t" + tag.getTag() + "\n\t\t" + tag.getTimestamp());
		}*/
		return retornar;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		if (operaciones == null){
			operaciones = new Stack<>();
		}
		VOOperacion op = new VOOperacion();
		op.setOperacion(nomOperacion);
		op.setTimestampInicio(tinicio);
		op.setTimestampFin(tfin);

		operaciones.push(op);
	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		ILista<VOOperacion> ret = new ListaEncadenada<VOOperacion>();
		Stack<VOOperacion> temp = operaciones.clone();
		VOOperacion operacion = null;
		while ((operacion = temp.pop()) != null) {
			ret.agregarElementoFinal(operacion);
		}
		return ret;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		operaciones = new Stack<>();
	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		ILista<VOOperacion> ret = new ListaEncadenada<>();
		Stack<VOOperacion> temp = operaciones;
		for (int i = 0; i < n; i++) {
			if (operaciones.size() > 0){
				ret.agregarElementoFinal(temp.pop());
			}
		}
		return ret;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		for (int i = 0; i < n; i++) {
			if (operaciones.size() > 0) {
				operaciones.pop();
			}
		}
	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		ultimoId++;
		VOPelicula nueva = new VOPelicula();
		nueva.setTitulo(titulo);
		nueva.setAgnoPublicacion(agno);
		ListaEncadenada<String> gens = new ListaEncadenada<>();
		for (String gen:generos) {
			gens.agregarElementoFinal(gen);
		}
		nueva.setGenerosAsociados(gens);
		peliculas.agregarElementoFinal(nueva);
		actualizarGeneros();
		return ultimoId;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		VORating nuevo = new VORating();
		nuevo.setTimestamp(System.currentTimeMillis());
		nuevo.setIdPelicula(idPelicula);
		nuevo.setIdUsuario(idUsuario);
		nuevo.setRating(rating);
		for (VOPelicula pelicula: peliculas) {
			if (pelicula.getIdPelicula() == (long)idPelicula){
				double ratingAnterior = pelicula.getPromedioRatings();
				int numRatings = pelicula.getNumeroRatings();
				pelicula.setPromedioRatings(((ratingAnterior*numRatings)+rating)/numRatings+1);
			}
		}
		ratings.agregarElementoFinal(nuevo);
		cargarUsuariosGenero();
	}
	
	/*//metodo para probar el ordenamiento
	public void imprimirPeliculasEnOrden() {
		ListaEncadenada<VOPelicula> pels = (ListaEncadenada<VOPelicula>) peliculas;
		Sort<VOPelicula> abd = new Sort<>();
		long init = System.nanoTime();
		pels = (ListaEncadenada<VOPelicula>) abd.mergeSort(pels, 0);
		long fin = System.nanoTime();
		int i = 0;
		/*for (VOPelicula pe : pels) {
			if (i%50 == 0) {
				System.out.println(i + ": " + pe.toString());
			}
			i++;
		}
		System.out.println("Tiempo total de ordenamiento: " + (fin-init)/1000000 + " ms");
	}*/

	public ILista<VOGeneroPelicula> generoPel() {
		return generoPelicula;
	}
}
