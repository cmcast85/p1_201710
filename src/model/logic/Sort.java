package model.logic;

import api.IComparable;
import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;

public class Sort <T extends IComparable<T>>{

    public ILista<T> mergeSort(ILista<T> arreglo, int criterio) {
    	arreglo.restablecerActual();
    	ListaEncadenada<T> aRetornar = new ListaEncadenada<>();
    	int tamano = arreglo.darNumeroElementos();
        if (tamano > 1) {

            int size1 = (int)Math.floor(tamano / 2);
            int size2 = tamano - size1;

            ListaEncadenada<T> left = new ListaEncadenada<>();
            ListaEncadenada<T> right = new ListaEncadenada<>();
            for (int i = 0; i < size1; i++) {

                left.agregarElementoFinal(arreglo.darElementoPosicionActual());
                arreglo.avanzarSiguientePosicion();

            }

            for (int i = 0; i < size2; i++)  {

                right.agregarElementoFinal(arreglo.darElementoPosicionActual());
                arreglo.avanzarSiguientePosicion();

            }

            left = (ListaEncadenada<T>) mergeSort(left, criterio);

            right = (ListaEncadenada<T>) mergeSort(right, criterio);
            aRetornar = (ListaEncadenada<T>) merge(left, right, criterio);

        } else return arreglo;
        
        return aRetornar;
    }

    private ILista<T> merge(ILista<T> left, ILista<T> right, int criterio) {
    	
    	left.restablecerActual();
    	right.restablecerActual();
    	
    	ILista<T> result = new ListaEncadenada<T>();
    	
    	int tam = left.darNumeroElementos() + right.darNumeroElementos();
    	
        int i1 = 0;
        int i2 = 0;

        for (int i = 0; i < tam; i++) {
            if (i2 >= right.darNumeroElementos() || (i1 < left.darNumeroElementos() && left.darElementoPosicionActual().compareTo(right.darElementoPosicionActual(), criterio) <= 0)) {
                result.agregarElementoFinal(left.darElementoPosicionActual());
                left.avanzarSiguientePosicion();
                i1++;
            }
 
            else {
            	result.agregarElementoFinal(right.darElementoPosicionActual());
            	right.avanzarSiguientePosicion();
            	i2++;
            }
        }
        return result;
    }
 }