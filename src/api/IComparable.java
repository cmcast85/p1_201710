package api;

public interface IComparable<T> {
	public int compareTo(T o, int criterio);
}
