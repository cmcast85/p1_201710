package view;

import model.data_structures.ListaEncadenada;
import model.logic.SistemaRecomendacionPeliculas;
import model.vo.VOOperacion;

public class Vista {
	
	//metodo para probar el ordenamiento
	public static void main (String[] args) {
		SistemaRecomendacionPeliculas srp = new SistemaRecomendacionPeliculas();
		long init2 = System.currentTimeMillis();
		long init = System.nanoTime();
		srp.cargarPeliculasSR("data/movies.csv");
		long fin = System.nanoTime();
		srp.agregarOperacionSR("cargarPeliculas", init2, System.currentTimeMillis());
		
		
		double peliculas = (fin-init)/1000000;
		init2 = System.currentTimeMillis();
		init = System.nanoTime();
		srp.cargarRatingsSR("data/ratings.csv");
		fin = System.nanoTime();
		srp.agregarOperacionSR("cargarRatings", init2, System.currentTimeMillis());
		
		
		double ratings = (fin-init)/1000000;
		init2 = System.currentTimeMillis();
		init = System.nanoTime();
		srp.cargarTagsSR("data/tags.csv");
		fin = System.nanoTime();
		srp.agregarOperacionSR("cargarTags", init2, System.currentTimeMillis());
		double tags = (fin-init)/1000000;
		System.out.println("Tiempo total de carga de archivos: \nPeliculas: " + peliculas + " ms \nRatings: " + ratings + " ms \nTags: " + tags + " ms \n");
		
		
		System.out.println("--------------------------------\t1A:peliculasPopulares\t--------------------------------");
		init2 = System.currentTimeMillis();
		srp.peliculasPopularesSR(5);
		srp.agregarOperacionSR("1A: peliculasPopulares", init2, System.currentTimeMillis());
		System.out.println("--------------------------------\t2A:catalogoPeliculasOrdenado\t--------------------------------");
		init2 = System.currentTimeMillis();
		srp.catalogoPeliculasOrdenadoSR();
		srp.agregarOperacionSR("2A: catalogoPeliculasOrdenado", init2, System.currentTimeMillis());
		System.out.println("--------------------------------\t3A:recomendarGenero\t--------------------------------");
		init2 = System.currentTimeMillis();
		srp.recomendarGeneroSR();
		srp.agregarOperacionSR("3A: recomendarGenero", init2, System.currentTimeMillis());
		System.out.println("--------------------------------\t4A:opinionRatingsGenero\t--------------------------------");
		init2 = System.currentTimeMillis();
		srp.opinionRatingsGeneroSR();
		srp.agregarOperacionSR("4A: opinionRatingsGenero", init2, System.currentTimeMillis());
		System.out.println("--------------------------------\t5A:recomendarPeliculas\t--------------------------------");
		init2 = System.currentTimeMillis();
		srp.recomendarPeliculasSR("data/recomendaciones.csv", 5);
		srp.agregarOperacionSR("5A: recomendarPeliculas", init2, System.currentTimeMillis());
		System.out.println("--------------------------------\t6A:ratingsPelicula\t--------------------------------");
		init2 = System.currentTimeMillis();
		srp.ratingsPeliculaSR(1);
		srp.agregarOperacionSR("6A: ratingsPelicula", init2, System.currentTimeMillis());
		System.out.println("--------------------------------\t1B:usuariosActivos\t--------------------------------");
		init2 = System.currentTimeMillis();
		srp.usuariosActivosSR(10);
		srp.agregarOperacionSR("1B: usuariosActivos", init2, System.currentTimeMillis());
		System.out.println("--------------------------------\t2B:catalogoUsuariosOrdenado\t--------------------------------");
		init2 = System.currentTimeMillis();
		srp.catalogoUsuariosOrdenadoSR();
		srp.agregarOperacionSR("2B: catalogoUsuariosOrdenado", init2, System.currentTimeMillis());
		System.out.println("--------------------------------\t3B:recomendarTagsGenero\t--------------------------------");
		init2 = System.currentTimeMillis();
		srp.recomendarTagsGeneroSR(5);
		srp.agregarOperacionSR("3B: recomendarTagsGenero", init2, System.currentTimeMillis());
		System.out.println("--------------------------------\t4B:opinionTagsGenero\t--------------------------------");
		init2 = System.currentTimeMillis();
		srp.opinionTagsGeneroSR();
		srp.agregarOperacionSR("4B: opinionTagsGenero", init2, System.currentTimeMillis());
		System.out.println("--------------------------------\t5B:recomendarUsuarios\t--------------------------------");
		init2 = System.currentTimeMillis();
		srp.recomendarUsuariosSR("data/recomendaciones.csv", 5);
		srp.agregarOperacionSR("5B: recomendarUsuarios", init2, System.currentTimeMillis());
		System.out.println("--------------------------------\t6B:tagsPelicula\t--------------------------------");
		init2 = System.currentTimeMillis();
		srp.tagsPeliculaSR(4973);
		srp.agregarOperacionSR("6B: tagsPelicula", init2, System.currentTimeMillis());
		
		ListaEncadenada<VOOperacion> operaciones = (ListaEncadenada<VOOperacion>) srp.darHistoralOperacionesSR();
		for(VOOperacion operacion : operaciones) {
			System.out.println(operacion.getOperacion() + " Tiempo transcurrido: " + (operacion.getTimestampFin() - operacion.getTimestampInicio()) + " ms");
		}
		System.out.println("---- ultimas 3 operaciones ----");
		operaciones = (ListaEncadenada<VOOperacion>) srp.darUltimasOperaciones(3);
		for(VOOperacion operacion : operaciones) {
			System.out.println(operacion.getOperacion() + " Tiempo transcurrido: " + (operacion.getTimestampFin() - operacion.getTimestampInicio()) + " ms");
		}
		System.out.println("---- se borran 4 operaciones ----");
		srp.borrarUltimasOperaciones(4);
		operaciones = (ListaEncadenada<VOOperacion>) srp.darUltimasOperaciones(3);
		for(VOOperacion operacion : operaciones) {
			System.out.println(operacion.getOperacion() + " Tiempo transcurrido: " + (operacion.getTimestampFin() - operacion.getTimestampInicio()) + " ms");
		}
		System.out.println("---- se borran todas las operaciones ----");
		srp.limpiarHistorialOperacionesSR();
		srp.agregarOperacionSR("fin!!", System.currentTimeMillis(), System.currentTimeMillis());
		operaciones = (ListaEncadenada<VOOperacion>) srp.darUltimasOperaciones(1);
		for(VOOperacion operacion : operaciones) {
			System.out.println(operacion.getOperacion() + " Tiempo transcurrido: " + (operacion.getTimestampFin() - operacion.getTimestampInicio()) + " ms");
		}
	}
}
