package test;


import java.util.Comparator;
import java.util.Iterator;
import java.util.UUID;
 
import junit.framework.TestCase;
import model.data_structures.*;
import model.logic.*;
import model.vo.*;
 
public class SistemaTest extends TestCase
{
 
    private SistemaRecomendacionPeliculas sr;
 
    
 
    public void setupScenario2(){
        sr = new SistemaRecomendacionPeliculas();
        sr.cargarPeliculasSR("./data/movies.csv");
        sr.cargarRatingsSR("./data/ratings.csv");
        sr.cargarTagsSR("./data/tags.csv");
    }
 
    public void testCargarTodo(){
        setupScenario2();
    }
 
    public void testPeliculasPopulares1A(){
        setupScenario2();
        ILista<VOGeneroPelicula> lista = sr.peliculasPopularesSR(100);
        for(VOGeneroPelicula gen: lista){
            assertTrue(gen.getPeliculas().darNumeroElementos() <= 100);
            VOPelicula ant = null;
            for(int i = 0; i < gen.getPeliculas().darNumeroElementos(); i ++){
                VOPelicula pel = gen.getPeliculas().darElemento(i) ;
                if(ant != null){
                    assertTrue(pel.getNumeroRatings() <= ant.getNumeroRatings());
                }
                ant = pel;
            }
        }
 
    }
 
    public void testOrdenarPeliculas2A(){
        setupScenario2();
        ILista<VOPelicula> lista = sr.catalogoPeliculasOrdenadoSR();
        VOPelicula ant = null;
        for(VOPelicula pel: lista){
            //System.out.println(pel.getTitulo() + ", " + pel.getAgnoPublicacion());
            if(ant != null)
                assertTrue(ant.getAgnoPublicacion()<= pel.getAgnoPublicacion());
            ant = pel;
        }
    }
 
    public void testRecomendarGenero3A(){
        setupScenario2();
        ILista<VOGeneroPelicula> listaMejores = sr.recomendarGeneroSR();
        ILista<VOGeneroPelicula> listaGrande = sr.generoPel() ;
        Iterator<VOGeneroPelicula> iterMejores = listaMejores.iterator();
        for(VOGeneroPelicula genPel: listaGrande){
            VOPelicula mejor = iterMejores.next().getPeliculas().darElemento(0);
            for(VOPelicula pel: genPel.getPeliculas()){
                assertTrue(mejor.getPromedioRatings() >= pel.getPromedioRatings());
            }
        }
    }
 
    public void testOpinionRatingGenero4A(){
        setupScenario2();
        ILista<VOGeneroPelicula> lista = sr.opinionRatingsGeneroSR();
        for(VOGeneroPelicula genPel: lista){
            VOPelicula ant = null;
            for(VOPelicula pel: genPel.getPeliculas()){
                if(ant !=null)
                    assertTrue(pel.getAgnoPublicacion() >= ant.getAgnoPublicacion());
                ant = pel;
            }
        }
    }
 
    public void testRecomendarPeliculas5A(){
        setupScenario2();
        //System.out.println(test.getIdPelicula());
        ILista<VOPeliculaPelicula> lista = sr.recomendarPeliculasSR("./data/recomendacionesTest.csv", 3);
 
        for(VOPeliculaPelicula pelPel: lista){
            VOPelicula ant = null;
            for(VOPelicula pel: pelPel.getPeliculasRelacionadas()){
                if(ant!=null){
                    double difAbsActual = Math.abs(pelPel.getRating() - pel.getPromedioRatings());
                    double difAbsAnt = Math.abs(pelPel.getRating() - ant.getPromedioRatings());
                    assertTrue(difAbsAnt <= difAbsActual);
                }
                assertTrue(pel.getPromedioRatings() <= pelPel.getRating()+5 && pel.getPromedioRatings() >= pelPel.getRating()-5);
                ant = pel;
            }
 
        }
    }
 
    public void testRatingsPeliculas6A(){
        setupScenario2();
        ILista<VORating> lista = sr.ratingsPeliculaSR(10);
        VORating ant = null;
        for(VORating pel: lista){
            //System.out.println((long) pel.getTimestamp());
            if(ant != null)
                assertTrue(ant.getTimestamp() > pel.getTimestamp());
            ant = pel;
        }
    }
 
    public void testUsuariosActivosSR1B()
    {
        setupScenario2();
        ILista<VOGeneroUsuario> vgul = sr.usuariosActivosSR(10);
        Iterator<VOGeneroUsuario> itvgu3 = vgul.iterator();
 
        for(int i = 0; i < vgul.darNumeroElementos(); i++)
        {
            VOGeneroUsuario vgu3 = itvgu3.next();
            ILista<VOUsuarioConteo> vuc = vgu3.getUsuarios();
            assertTrue(vuc.darNumeroElementos()<=10);
 
            Iterator<VOUsuarioConteo> itvuc = vuc.iterator();
            VOUsuarioConteo vucfirst = null;
            VOUsuarioConteo vuclast = null;
 
            
            if(vuc.darNumeroElementos()>0)
            {
                vuclast = itvuc.next();
            }
            for (int j = 0; j < vuc.darNumeroElementos()-1; j++)
            {
                vucfirst = vuclast;
                vuclast = itvuc.next();
                assertTrue(vucfirst.compareTo(vuclast, 0) <=0);
            }
        }
    }
 
    public void testCatalogoUsuariosOrdenadoSR2B()
    {
        setupScenario2();
        ILista<VOUsuario> vul = sr.catalogoUsuariosOrdenadoSR();
 
        Iterator<VOUsuario> itvu1 = vul.iterator();
 
        VOUsuario vucfirst = null;
        VOUsuario vuclast = null;
 
        
 
        if(vul.darNumeroElementos()>0)
        {
            vuclast = itvu1.next();
        }
        for(int i = 0; i < vul.darNumeroElementos()-1; i++)
        {
            vucfirst = vuclast;
            vuclast = itvu1.next();
            assertTrue(vucfirst.compareTo(vuclast, 0) <=0);
        }
    }
 
    public void testRecomendarTagsGeneroSR3B()
    {
        setupScenario2();
        ILista<VOGeneroPelicula> vgpl = sr.recomendarTagsGeneroSR(10);
 
        Iterator<VOGeneroPelicula> itvgpl = vgpl.iterator();
 
        for (int i = 0; i < vgpl.darNumeroElementos(); i++)
        {
            VOGeneroPelicula vgp = itvgpl.next();
            ILista<VOPelicula> vpl = vgp.getPeliculas();
 
            assertTrue(vpl.darNumeroElementos()<=10);
            Iterator<VOPelicula> itvpl = vpl.iterator();
 
            VOPelicula vucfirst = null;
            VOPelicula vuclast = null;
 
            
            if(vpl.darNumeroElementos()>0)
            {
                vuclast = itvpl.next();
            }
            for (int j = 0; j < vpl.darNumeroElementos()-1; j++)
            {
                vucfirst = vuclast;
                vuclast = itvpl.next();
                assertTrue(vucfirst.compareTo(vuclast, 0) <=0);   
            }
        }
    }
 
    public void testOpinionTagsGeneroSR4B()
    {
        setupScenario2();
        ILista<VOUsuarioGenero> respuesta = sr.opinionTagsGeneroSR();
        Iterator<VOUsuarioGenero> itvguk = respuesta.iterator();
        for(int i = 0; i < respuesta.darNumeroElementos(); i++)
        {
            VOUsuarioGenero vugk = itvguk.next();
            ILista <VOGeneroTag> vgtlo = vugk.getListaGeneroTags();
 
            Iterator<VOGeneroTag> itvgtk = vgtlo.iterator();
            for(int j = 0; j < vgtlo.darNumeroElementos(); j++)
            {
                VOGeneroTag vgtp = itvgtk.next();
                ILista<String> vgtlt = vgtp.getTags();
 
                Iterator<String> itvpl = vgtlt.iterator();
 
                String vucfirst = null;
                String vuclast = null;
 
                if(vgtlt.darNumeroElementos()>0)
                {
                    vuclast = itvpl.next();
                }
                for(int k1 = 0; k1 <vgtlt.darNumeroElementos()-1; k1++)
                {
                    vucfirst = vuclast;
                    vuclast = itvpl.next();
                    assertTrue(vucfirst.compareTo(vuclast)<=0);   
                }
            }
        }
    }
 
    public void testRecomendarUsuariosSR5B()
    {
        setupScenario2();
        ILista<VOPeliculaUsuario> respuesta = sr.recomendarUsuariosSR("./data/recomendaciones.csv", 10);
        Iterator<VOPeliculaUsuario> itvpu4 = respuesta.iterator();
        for (int i = 0; i < respuesta.darNumeroElementos(); i++)
        {
            VOPeliculaUsuario vpu5 = itvpu4.next();
 
            ILista<VOUsuario> vul6 = vpu5.getUsuariosRecomendados();
            Iterator<VOUsuario> itvou7 = vul6.iterator();
 
            assertTrue(vul6.darNumeroElementos()<=10);
 
            VOUsuario vucfirst = null;
            VOUsuario vuclast = null;
            
            if(vul6.darNumeroElementos()>0)
            {
                vuclast = itvou7.next();
            }
            for(int k1 = 0; k1 <vul6.darNumeroElementos()-1; k1++)
            {
                vucfirst = vuclast;
                vuclast = itvou7.next();
                assertTrue(vucfirst.compareTo(vuclast, 0)<=0);   
            }
        }
    }
 
    public void testTagsPeliculaSR6B()
    {
        setupScenario2();
        ILista<VOTag> resp = sr.tagsPeliculaSR(260);
        Iterator<VOTag> itvt = resp.iterator();
 
        VOTag vucfirst = null;
        VOTag vuclast = null;
 
        
 
        if(resp.darNumeroElementos()>0)
        {
            vuclast = itvt.next();
        }
        for(int k1 = 0; k1 <resp.darNumeroElementos()-1; k1++)
        {
            vucfirst = vuclast;
            vuclast = itvt.next();
            assertTrue(vucfirst.compareTo(vuclast, 0) <=0);   
        }
 
    }
   
    public void testAgregarOperacion(){
        setupScenario2();
        sr.agregarOperacionSR("testOperacion", 1, 1);
        ILista<VOOperacion> listaOp = sr.darHistoralOperacionesSR();
        assertTrue(listaOp.darElemento(0).getOperacion().equals("testOperacion"));
        for(VOOperacion op: listaOp){
            assertNotNull(op);
        }
        assertEquals(4, listaOp.darNumeroElementos());
    }
   
    public void testDarHistorialOperacion1C(){
        setupScenario2();
        ILista<VOOperacion> listaOp = sr.darHistoralOperacionesSR();
        for(VOOperacion op: listaOp){
            assertNotNull(op);
        }
        assertEquals(3, listaOp.darNumeroElementos());
    }
   
    public void testLimpiarHistorialOperaciones2C(){
        setupScenario2();
        sr.limpiarHistorialOperacionesSR();
        ILista<VOOperacion> lista = sr.darHistoralOperacionesSR();
        assertTrue(lista.darNumeroElementos() == 0);
    }
   
    public void testDarUltimasOperaciones2C(){
        setupScenario2();
        ILista<VOOperacion> lista = sr.darUltimasOperaciones(2);
        assertTrue(lista.darNumeroElementos() == 2);
        assertEquals("cargarRatings", lista.darElemento(1).getOperacion());
        assertEquals("cargarTags", lista.darElemento(0).getOperacion());
    }
   
    public void testBorrarUltimasOperaciones4C(){
        setupScenario2();
        sr.borrarUltimasOperaciones(2);
        ILista<VOOperacion> lista = sr.darHistoralOperacionesSR();
        assertEquals(1, lista.darNumeroElementos());
        assertTrue(lista.darElemento(0).getOperacion() == "cargarPeliculas");
    }
   
   
 
    public void testListaEncadenada(){
        ListaEncadenada<String> lista = new ListaEncadenada<>();
        lista.agregarElementoFinal ("Hola");
        lista.eliminarElemento(0);
        assertTrue(lista.darNumeroElementos() == 0);
        lista.agregarElementoFinal("Ola");
        assertEquals("Ola", lista.darElemento(0));
        lista.agregarElementoFinal("alo");
        lista.agregarElementoFinal("kmas");
        lista.eliminarElemento(0);
        assertTrue(lista.darNumeroElementos() == 2);
   
    }
 
    /**
     * este es basicamente para medir el tiempo que toma hacer todas las operaciones
     */
    public void testDeTests(){
        setupScenario2();
        sr.peliculasPopularesSR(100);
        sr.catalogoPeliculasOrdenadoSR();
        sr.recomendarGeneroSR();
        sr.opinionRatingsGeneroSR();
        sr.recomendarPeliculasSR("./data/recomendaciones.csv", 15);
        sr.ratingsPeliculaSR(10);
        sr.usuariosActivosSR(100);
        sr.catalogoUsuariosOrdenadoSR();
        sr.recomendarTagsGeneroSR(20);
        sr.opinionTagsGeneroSR();
        sr.recomendarUsuariosSR("./data/recomendaciones.csv", 15);
        sr.tagsPeliculaSR(10);
    }
 
}
